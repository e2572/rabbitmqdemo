﻿using RabbitMQ.Client;
using RabbitMqDemo.Service.QueuePattern;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("please input  queue public pattern : 1 workerqueue,2 fanoutqueue,3 routingqueue");
        var pattern = Console.ReadLine();
        switch (pattern)
        {
            case "1":
                WorkerQueuePub();
                break;
            case "2":
                FanoutQueuePub();
                break;
            case "3":
                RoutingQueuePub();
                break;
            default:
                Console.WriteLine("pattern info error..");
                break;
        }
        Console.WriteLine("Server exit");
        Console.ReadKey();
    }

    static void WorkerQueuePub()
    {
        var workerQueue = new WorkerQueue();
        Console.WriteLine("Please input message : ");
        var message = Console.ReadLine();
        while (message != "q")
        {
            workerQueue.Publish(message ?? string.Empty);
            message = Console.ReadLine(); 
        }
    }

    static void FanoutQueuePub()
    {
        var fanoutQueue = new FanoutQueue();
        Console.WriteLine("Please input message : ");
        var message = Console.ReadLine();
        while (message != "q")
        {
            fanoutQueue.Publish(message ?? string.Empty);
            message = Console.ReadLine();
        }      
    }

    static void RoutingQueuePub()
    {
        var fanoutQueue = new RoutingQueue();
        Console.WriteLine("Please input message : ");
        var message = Console.ReadLine();
        while (message != "q")
        {
            fanoutQueue.Publish(message ?? string.Empty);
            message = Console.ReadLine();
        }
    }
}