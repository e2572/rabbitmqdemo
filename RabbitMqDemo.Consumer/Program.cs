﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using RabbitMqDemo.Service;
using System.ComponentModel;
using System.Text;
using RabbitMqDemo.Service.QueuePattern;

namespace RabbitMqDemo.Consumer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("please input  queue subscriber pattern : 1 workerqueue,2 fanoutqueue,3 routingqueue");
            var pattern = Console.ReadLine();
            switch (pattern)
            {
                case "1":
                    WorkerQueueConsume();
                    break;
                case "2":
                    FanoutQueueConsume();
                    break;

                case "3":
                    RoutingQueueConsume();
                    break;
                default:
                    Console.WriteLine("pattern info error..");
                    break;
            }
            Console.ReadKey();
        }

        static void WorkerQueueConsume()
        {
            // task worker 1
            Task.Factory.StartNew(() => {
                try
                {
                    var workerQueue = new WorkerQueue();
                    workerQueue.Consume("task one",0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            },TaskCreationOptions.LongRunning);

            // task worker 2
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var workerQueue = new WorkerQueue();
                    workerQueue.Consume("task two", 2, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            },TaskCreationOptions.LongRunning);
        }

        static void FanoutQueueConsume()
        {
            // task consume 1
            Task.Factory.StartNew(() => {
                try
                {
                    var workerQueue = new FanoutQueue();
                    workerQueue.Consume("task one", 0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }, TaskCreationOptions.LongRunning);

            // task consume 2
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var workerQueue = new FanoutQueue();
                    workerQueue.Consume("task two", 2, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }, TaskCreationOptions.LongRunning);
        }

        static void RoutingQueueConsume()
        {
            // task consume 1
            Task.Factory.StartNew(() => {
                try
                {
                    var routingQueue = new RoutingQueue();
                    routingQueue.Consume("task one", routingQueue.QueueLogAll, routingQueue.LogTypes, 0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }, TaskCreationOptions.LongRunning);

            // task consume 2
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var routingQueue = new RoutingQueue();
                    routingQueue.Consume("task two", routingQueue.QueueErrorLog, new string[] { "error" }, 2, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }, TaskCreationOptions.LongRunning);
        }
    }
}
