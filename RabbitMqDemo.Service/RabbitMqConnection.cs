﻿namespace RabbitMqDemo.Service
{
    using RabbitMQ.Client;

    public sealed class RabbitMqConnection
    {
        private RabbitMqConnection() { }

        private static IConnection? _connection;

        private static readonly object _lockObj = new object();

        public static IConnection GetConnection()
        {
            if (_connection == null)
            {
                lock (_lockObj)
                {
                    if (_connection == null)
                    {
                        var factory = new ConnectionFactory { Uri = new Uri("amqp://rabbit:Abc123456@127.0.0.1:5672/") };
                        _connection = factory.CreateConnection();
                    }                   
                }
            }
            return _connection;
        }
    }
}
