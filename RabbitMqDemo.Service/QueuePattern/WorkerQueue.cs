﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMqDemo.Service.QueuePattern
{
    /// <summary>
    /// https://www.rabbitmq.com/tutorials/tutorial-two-dotnet
    /// </summary>
    public class WorkerQueue
    {
        private const string QueueName = "Worker_Queue";

        private readonly IModel _channel;

        public WorkerQueue()
        {
            _channel = RabbitMqConnection.GetConnection().CreateModel();
            _channel.QueueDeclare(queue: QueueName,
                 durable: true, // make data is durable
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);

            //fair Dispatch >> prefetchCount: 1
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
        }

        /// <summary>
        /// publish message by worker queue
        /// </summary>
        /// <param name="message"></param>
        public void Publish(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            // make data is durable
            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;

            _channel.BasicPublish(exchange: string.Empty,
                                 routingKey: QueueName, // the routingkey must be the queuename when using workerqueue without exchange
                                 basicProperties: properties,
                                 body: body);
        }

        /// <summary>
        /// Consume message
        /// </summary>
        /// <param name="consumerName">consumerName</param>
        /// <param name="delay">delay time (second)</param>
        /// <param name="autoExit">autoExit</param>
        public void Consume(string consumerName, int delay = 0, bool autoExit = false)
        {
            Console.WriteLine($" {consumerName} Waiting for messages with delay {delay} second.");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($" {consumerName} Received {message}");

                // test message ack if app exit
                if (autoExit && message.ToLower().Contains("exit"))
                {
                    //Environment.Exit(0);
                    Console.WriteLine($"{consumerName} Auto exit..");
                    throw new Exception($"{consumerName} Auto exit..");
                }

                Thread.Sleep(1000 * delay);

                //If a consumer dies (its channel is closed, connection is closed, or TCP connection is lost) without sending an ack, RabbitMQ will understand that a message wasn't processed fully and will re-queue it.
                //Or A timeout (30 minutes by default) is enforced on consumer delivery acknowledgement
                _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false); // ack settings
            };

            _channel.BasicConsume(queue: QueueName,
                                 autoAck: false, // false is manually ack, common use
                                 consumer: consumer);
        }
    }
}
