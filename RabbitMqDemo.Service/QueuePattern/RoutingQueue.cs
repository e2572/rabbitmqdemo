﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMqDemo.Service.QueuePattern
{
    public class RoutingQueue
    {

        private const string exchangeName = "Direct_Exchange";
        public string QueueLogAll = "Direct_AllLog";
        public string QueueErrorLog = "Dirext_ErrorLog";
        public string[] LogTypes = new string[] { "debug", "info", "warn", "error" };
        private readonly IModel _channel;
        
        public RoutingQueue()
        {
            _channel = RabbitMqConnection.GetConnection().CreateModel();
            _channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Direct);

          
        }

        /// <summary>
        /// publish message by worker queue
        /// </summary>
        /// <param name="routingKey"></param>
        /// <param name="message"></param>
        public void Publish(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            // make data is durable
            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;

            foreach (var routingKey in LogTypes)
            {
                _channel.BasicPublish(exchange: exchangeName, //publish message to specail exchange
                                routingKey: routingKey,
                                basicProperties: properties,
                                body: body);
            } 
        }

        /// <summary>
        /// Consume message
        /// </summary>
        /// <param name="consumerName">consumerName</param>
        /// <param name="routings">routingKeys</param>
        /// <param name="delay">delay time (second)</param>
        /// <param name="autoExit">autoExit</param>
        public void Consume(string consumerName,string queueName, string[] routingKeys, int delay = 0, bool autoExit = false)
        {
            Console.WriteLine($" {consumerName} Waiting for messages with delay {delay} second.");

            _channel.QueueDeclare(queue: queueName,
                  durable: true, // make data is durable
                  exclusive: false,
                  autoDelete: false,
                  arguments: null);


            // biding different routing
            foreach (var severity in routingKeys)
            {
                _channel.QueueBind(queue: queueName,
                                  exchange: exchangeName,
                                  routingKey: severity);
            }

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($" {consumerName} Received {message}");

                // test message ack if app exit
                if (autoExit && message.ToLower().Contains("exit"))
                {
                    //Environment.Exit(0);
                    Console.WriteLine($"{consumerName} Auto exit..");
                    throw new Exception($"{consumerName} Auto exit..");
                }

                Thread.Sleep(1000 * delay);

                _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false); // ack settings

            };

            _channel.BasicConsume(queue: queueName,
                                 autoAck: false,
                                 consumer: consumer);
        }
    }
}
