﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMqDemo.Service.QueuePattern
{
    public class FanoutQueue
    {
        private const string exchangeName = "Fanout_Exchange";

        private readonly IModel _channel;

        public FanoutQueue()
        {
            _channel = RabbitMqConnection.GetConnection().CreateModel();
            _channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Fanout);
        }

        /// <summary>
        /// publish message by worker queue
        /// </summary>
        /// <param name="message"></param>
        public void Publish(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: exchangeName, //publish message to specail exchange
                                 routingKey: string.Empty, 
                                 basicProperties: null,
                                 body: body);
        }

        /// <summary>
        /// Consume message
        /// </summary>
        /// <param name="consumerName">consumerName</param>
        /// <param name="delay">delay time (second)</param>
        /// <param name="autoExit">autoExit</param>
        public void Consume(string consumerName, int delay = 0, bool autoExit = false)
        {
            Console.WriteLine($" {consumerName} Waiting for messages with delay {delay} second.");

            // declare a server-named queue (random name)
            var queueName = _channel.QueueDeclare().QueueName;
            _channel.QueueBind(queue: queueName,
                              exchange: exchangeName,
                              routingKey: string.Empty);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine($" {consumerName} Received {message}");

                // test message ack if app exit
                if (autoExit && message.ToLower().Contains("exit"))
                {
                    //Environment.Exit(0);
                    Console.WriteLine($"{consumerName} Auto exit..");
                    throw new Exception($"{consumerName} Auto exit..");
                }

                Thread.Sleep(1000 * delay);

            };

            _channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}
